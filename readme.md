# Blank HTML5 Starter for CodeKit with Bourbon &  Bourbon Neat

This is a blank starter kit to use with CodeKit and .kit files.

I created this to help me build out prototypes a litte quicker and to keep only the necessary deployment files in a `publish` folder so that I'm not pushing up a bunch of un-necessary build files that are not needed for viewing, testing, or production. I know there are other things out there that could do the same thing (like LiveWires) but I wanted something that was pretty much blank from the get go and I didn't want to spend time removing things that I knew that I wasn't going to use. Plus, I use CodeKit (for now) to handle all of my pre-processing so since I paid for it, I might as well get the most out of it.

It comes pre-packaged with this stuff:  
- Normalize
- jQuery
- Modernizr
- Setup to use Bourbon's mixin library and Bourbon Neat for the responsive grid.
- I also have the SCSS file for .fitgrd (https://github.com/jayalai/fitgrd) in there as well. It's commented out in the `style.scss` file. I figured I'd keep it in there in case someone needed something not as robust as Bourbon Neat.

I also have configured a `.content` class to kick on BLOKK font so that you can create blocked out sections of text opposed to using Lorum Ipsum or, in my case, use this for building out quick wireframes (shoutout goes to the peeps at [nGen](http://www.ngenworks.com/blog/live-wires-better-prototyping) and [Agnostic](http://www.agnostic.io/) for having that snippet in their [LiveWires](http://www.agnostic.io/livewires)). I'll talk about how to use it down [here](#blokk-font).


It's broken out into two parts; a `build` folder and a `publish` folder.

## Build Folder

This is the folder where you will make all of your edits.

All of the SCSS files are in the `scss` folder.

There is a blank `scripts.js` file in the js folder that is set to complie out to the `publish` folder. Using CodeKit, you can prepend/append files to this file.

I've broken up the `header.kit` and the `footer.kit` files into the `partials` folder. The `index.kit` file will pull those to files and then compile down to the `publish` folder.

## Publish Folder

The publish folder is where everything gets compiled out of CodeKit and should be "publish ready". That way you don't have to push up any of the build files when you are ready for deployment.

Any images you will want to put in the `imgs` folder.

Same thing goes for any other files that are necessary to your project (for example, the `font` folder).

## Blokk Font

To enable the blokk font, all you have to do is add `class="content"` to anything that you want to hide paragraphs and lists. That way you have it focus on getting the hierarchy organized and not have it filled with silly Lorum Ipsum.

## Recap
Like I mentioned, I created this kit to help me and help speed up my development efforts. If you find it useful, awesome! I'm stoked.  
If you think it sucks, then by all means, fork it and make it better. Or don't use it at all...I don't care and it's not gonna make me loose any sleep over it.
